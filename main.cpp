/*
 * File:   main.cpp
 * Author: pascal
 *
 * Created on 22. September 2016, 09:38
 */

#include <iostream>
#include <vector>
#include <string.h>
#include <fstream>

using namespace std;

/*
 *
 */

//      Escape code	Description
//      \n      	newline
//      \r              carriage return
//      \t              tab
//      \v              vertical tab
//      \b              backspace
//      \f              form feed (page feed)
//      \a              alert (beep)
//      \'              single quote (')
//      \"              double quote (")
//      \?              question mark (?)
//      \\              backslash (\)
       


// german = While Schleifen(Kopfgesteuert)

void while_loops() {
    int x = 1;
    while (x <= 4) {
        x++;
        cout << "I'm awesome!" << endl;
    }
}

// german = Do-While Schleifen(Fußgesteuert)

void do_while_loops() {
    int y = 4;
    do {
        int x = 2;
        x++;
        x - 2;
        cout << x << endl;
        y--;
    } while (y < 0);
}

// german = For Schleifen(Kopfgesteuert)

void for_loops() {
    int z;
    for (z = 2; z <= 8; z++) {
        cout << "z is " << z << endl;
    }
}

// german = Stringverkettung

void string_concentration() {
    string hello = "Hello ";
    string world = "World!";
    string hello_world = std::string(hello) + world;
    cout << hello_world;
}

// german = Vektoren

void vectors() {

    /*  Initialize vector of 10 copies of the integer 5 */
    vector<int> vectorOne(10, 5);

    /*  run through the vector and display each element, if possible */
    for (long index = 0; index < 11; ++index) {
        try {
            cout << "Element " << index << ": " << vectorOne.at(index) << endl;
        } catch (exception& e) {
            cout << "Element " << index << ": index exceeds vector dimensions." << endl;
        }
    }

}

void arrays() {
    int myarray[3] = {10, 20, 30};

    for (int i = 0; i < 3; ++i) {
        ++myarray[i];
    }

    for (int elem : myarray) {
        cout << elem << '\n';
    }
}

void pointer() {
    int var1 = 33;
    int *ptr1 = &var1;
    cout << "The hardware address of " << *ptr1 << " in memory is " << &ptr1 << endl;
}

// german = Char Zeichenfolgen

void char_sequences() {
    char myword1[] = {'H', 'e', 'l', 'l', 'o', '\0'};
    char myword2[] = {'W', 'o', 'r', 'l', 'd', '\0'};
    string myword12 = strcat(myword1, myword2);
    cout << myword12 << endl;
}

// german = Eingabe/+ Ausgabe von Dateien

void file_io() {
    fstream myFileStream("./test.txt", fstream::in | fstream::out | fstream::trunc);
    myFileStream << "Hello" << endl;
    myFileStream << "this" << endl;
    myFileStream << "is" << endl;
    myFileStream << "a" << endl;
    myFileStream << "test" << endl;
    myFileStream << "This is a single Line" << endl;
    myFileStream.close();
}

// german = dynamische Speicherzuweisung

void dynamic_memory() {
    int * foo;
    foo = new (nothrow) int [5];
    if (foo == nullptr) {
        // error assigning memory. Take measures.
        cout << "Yay we got nullpointer exception!" << endl;
    }
}

// german = objektorientierte Programierung

void object_oriented_programming() {

    class Dog {
    private:
        bool hungry;
        bool thirsty;

    public:
        string name;
        int age;

        // Constructor with default values for data members

        Dog(bool hungry_dog = true, bool thirsty_dog = true, string name_dog = "Bello", int age_dog = 8) {
            hungry = hungry_dog;
            thirsty = thirsty_dog;
            name = name_dog;
            age = age_dog;
        }

        void setHungry(bool hungry) {
            this->hungry = hungry;
        }

        bool getHungry() {
            return hungry;
        }

        void setThirsty(bool thirsty) {
            this->thirsty = thirsty;
        }

        bool getThirsty() {
            return thirsty;
        }
    };


    Dog Charlie = new Dog();
    Charlie.age = 3;
    Charlie.name = "Charlie";
    Charlie.setHungry(true);
    Charlie.setThirsty(false);

    cout << "The dog's name is " << Charlie.name << endl;
    cout << "He/She is " << Charlie.age << " Years old." << endl;

    if (Charlie.getHungry() == true) {
        cout << Charlie.name << " is hungry." << endl;
    } else {
        cout << Charlie.name << " is not hungry." << endl;
    }

    if (Charlie.getThirsty() == true) {
        cout << Charlie.name << " is thirsty." << endl;
    } else {
        cout << Charlie.name << " is not thirsty." << endl;
    }

}

// automatically detect variable types
// german = automatische Erkennung des Datentyps anhand anderer Variabeln
void deduction() {
    // when the variable gets initialized with a value
    int foo_1 = 12;
    auto bar_1 = foo_1;  // the same as: int bar_1 = foo_1;
    
    cout << foo_1 << " | " << bar_1 << endl;
    
    // when the variable gets not initialized with a value
    int foo_2 = 2;
    decltype(foo_2) bar_2;  // the same as: int bar_2; 
    
    cout << foo_2 << " | " << bar_2 << endl;
	
}

// enums (Enumerated Types) are similiar to arrays or array lists.
// in fact they behave as your own data type
// an enum returns a number in its list like an array.
// in this example "red" would have a value of "4"
void enums() {
    
    enum color { black, blue, white, red, yellow };
    
    color This_Will_Return_Black = black;
    color This_Will_Return_Blue = blue;
    color This_Will_Return_White = white;
    color This_Will_Return_Red = red;
    color This_Will_Return_Yellow = yellow;
    
    cout << This_Will_Return_Black << endl;
    cout << This_Will_Return_Blue << endl;
    cout << This_Will_Return_White << endl;
    cout << This_Will_Return_Red << endl;
    cout << This_Will_Return_Yellow << endl;
    
    if(This_Will_Return_Black == color {black} ) {
        cout << "Yay black is really black" << endl;
    }
    else {
        cout << "nope black is indeed not black." << endl;
    }
            
}

// a struct is a kind of object.
// A data structure is a group of data elements grouped together under one name.
// These data elements, known as members, can have different types and different lengths. 
void structs() {
    
    struct Person {
	int ID;
	string Name;
	int Age;
	int Height;
    };
    
    Person myPerson;
    
    myPerson.ID = 1337;
    myPerson.Name = "Clark";
    myPerson.Age = 24;
    myPerson.Height = 130;
    
    cout << "The Person's name is " << myPerson.Name << ". He / She is " << myPerson.Age << " Years old, got the ID " << myPerson.ID << " and is " << myPerson.Height << " meters tall." << endl; 
}

// Another mechanism to name constant values is the use of preprocessor definitions.
// like this : #define identifier replacement
// example:   #define PI 3.14159
            //#define NEWLINE '\n'
#define Hello "Hi m8" 
/* similiar to */  
const string Hello_ = "Hey m8";

// german = Hauptmethode

int main(int argc, char** argv) {
 
    cout << Hello << endl;
    remove("test.txt");

    while_loops();
    do_while_loops();
    for_loops();
    string_concentration();
    vectors();
    arrays();
    pointer();
    char_sequences();
    file_io();
    dynamic_memory();
    object_oriented_programming();
    deduction();
    enums();
    structs();

    return 0;
}
